from django.db import models

# Create your models here.

class Mind(models.Model):
    mind = models.CharField(primary_key=True, max_length=100)
    time = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f"{self.mind}"
    