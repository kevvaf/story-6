from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from . import views

app_name = 'App6'

urlpatterns = [
    path('', views.Index , name='Index'),
    path('challenge', include("challenge.urls"))
]