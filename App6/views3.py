from django.shortcuts import render
from django.http import HttpResponse
from .models import Mind
from .forms import Mindform

def Index(request):
    data = Mind.objects.all()
    if request.method=="Post":
        form = Mindform(request.POST)
        if form.is_valid():
            form.save
    
    return render(request,'Index.html',{'form':Mindform,'data':data})

# Create your views here.
