from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Mind
from .forms import Mindform

def Index(request):
    if request.method=="POST":
        frm = Mindform(request.POST)
        if frm.is_valid():
            mdl = Mind(mind=frm.cleaned_data["mind"])
            # mdl.mind = frm.cleaned_data["mind"]
            mdl.save()
        return redirect("/")
    frm = Mindform()
    mdl = Mind.objects.all()
    frm_dictio = {
        'Mindform':frm,
        'Mind':mdl
    }
    return render(request,'Index.html',frm_dictio)
    # {'mdl':Mindform,'data':data})

# Create your views here.
