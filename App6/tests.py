from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import Index
from .models import Mind

class Story6Test(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_index_func(self):
        found = resolve(reverse('App6:Index'))
        self.assertEqual(found.func, Index)  

    def test_models(self):
        Mind.objects.create(mind='pepew')
        hitungjumlah = Mind.objects.all().count()
        self.assertEqual(hitungjumlah,1)
        md = Mind.objects.get(mind = 'pepew')
        self.assertEqual(str(md),'pepew')


# Create your tests here.
