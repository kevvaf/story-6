from django import forms
from .models import Mind

class Mindform(forms.ModelForm):
    class Meta:
        model = Mind
        fields = '__all__'
    mind = forms.CharField(
        label = "",
        max_length = 30
    )