from django.shortcuts import render, redirect
from django.http import HttpResponse

def challenge(request):
    return render(request,'challenge.html')
