from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import challenge

class Story6Test(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_lab_3_using_index_func(self):
        found = resolve(reverse('challenge:challenge'))
        self.assertEqual(found.func, challenge)  



# Create your tests here.
