from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views 

app_name = 'challenge'

urlpatterns = [
    path('', views.challenge, name='challenge'),
]