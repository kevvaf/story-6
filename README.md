# Story 6
This Gitlab repository is the result of the work from **Kevin Muhammad Afiza Kurniawan**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/kevvaf/story-6/badges/master/pipeline.svg)](https://gitlab.com/kevvaf/story-6/commits/master)
[![coverage report](https://gitlab.com/kevvaf/story-6/badges/master/coverage.svg)](https://gitlab.com/kevvaf/story-6/commits/master)

## URL
This story can be accessed from [https://story6-kevin.herokuapp.com/](https://story6-kevin.herokuapp.com/)

## Author
**Kevin Muhammad Afiza Kurniawan** - [Kevin Muhammad Afiza Kurniawan]https://gitlab.com/kevvaf)